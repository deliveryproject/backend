package ru.terra.auto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.terra.auto.dto.OrderDto;
import ru.terra.auto.dto.UserDto;
import ru.terra.auto.service.OrderService;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by zhogin on 11/18/2016.
 */

@RestController
@RequestMapping(value = "api/orders")
@PreAuthorize("hasRole('USER')")
public class OrdersController {

    private OrderService orderService;

    @Autowired
    public OrdersController(OrderService userService) {
        this.orderService = userService;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<OrderDto> showAllUsers() {
        return orderService.findAll();
    }
}
