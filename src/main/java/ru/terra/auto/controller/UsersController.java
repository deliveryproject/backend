package ru.terra.auto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

import javax.validation.Valid;

import ru.terra.auto.dto.UserDto;
import ru.terra.auto.security.CurrentUser;
import ru.terra.auto.service.UserService;

/**
 * Created by zhogin on 11/18/2016.
 */

@RestController
@RequestMapping(value = "api/users")
@PreAuthorize("hasRole('USER')")
public class UsersController {

    private UserService userService;

    @Autowired
    public UsersController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/{username}", method = RequestMethod.GET)
    public ModelAndView showUser(@PathVariable String username) {
        UserDto userDTO = userService.getByUsername(username);
        return new ModelAndView("viewProfile", "user", userDTO);
    }

    @RequestMapping(value = "/{username}/edit", method = RequestMethod.GET)
    public ModelAndView showForEditUser(@PathVariable String username) {
        UserDto userDTO = userService.getByUsername(username);
        return new ModelAndView("editProfile", "user", userDTO);
    }

    @RequestMapping(value = "/{username}/edit", method = RequestMethod.POST)
    public ModelAndView updateUser(@PathVariable String username, @ModelAttribute("user") @Valid UserDto userDTO,
                                   Errors errors) {
        if (errors.hasErrors()) {
            return new ModelAndView("editProfile");
        }
        userService.update(userDTO);
        return new ModelAndView("redirect:/users/" + username);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<UserDto> showAllUsers() {
        return userService.findAll();
    }
}
