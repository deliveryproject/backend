package ru.terra.auto.dto;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;

/**
 * Created by zhogin on 11/30/2016.
 */
@Data
@EqualsAndHashCode
@NoArgsConstructor
public class LoginUserDto {

    @Size(min = 4, max = 20)
    private String username;

    @Size(min = 4, max = 25)
    private String password;

}
