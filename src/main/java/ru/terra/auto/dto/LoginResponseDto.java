package ru.terra.auto.dto;


import lombok.*;

/**
 * Created by zhogin on 11/30/2016.
 */
@Data
@EqualsAndHashCode
@RequiredArgsConstructor
public class LoginResponseDto {

    private final String accessToken;
    private final String username;

    private static String tokenType = "Bearer";

}
