package ru.terra.auto.config.security;

/**
 * Created by zhogin on 11/28/2016.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

import ru.terra.auto.security.CustomAuthenticationSuccessHandler;
import ru.terra.auto.security.JwtAuthenticationEntryPoint;
import ru.terra.auto.security.JwtAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private CustomAuthenticationSuccessHandler successHandler;

    @Autowired
    private JwtAuthenticationEntryPoint unauthorizedHandler;

    @Bean
    public JwtAuthenticationFilter jwtAuthenticationFilter() {
        return new JwtAuthenticationFilter();
    }

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /*http.authorizeRequests()
                .antMatchers("/api/auth*//**").permitAll()
         .antMatchers("/users/all").permitAll()//.access("hasRole('ROLE_ADMIN')")
         .antMatchers("/users/", "/users/{username}", "/cargo-requests/{cargoId}", "/vehicle-requests/add", "/vehicle-requests/{vehicleRequestId}").access("isFullyAuthenticated()")
         .antMatchers("/users/{username}/edit").access("@userAccessCheck.checkEditProfileAccess(authentication, request, #username)")
         .and().cors()
         .and().formLogin().loginPage("/login")
         .loginProcessingUrl("/j_spring_security_check")
         .failureUrl("/login?error")
         .usernameParameter("username").passwordParameter("password")
         .successHandler(successHandler)
         .and().exceptionHandling().accessDeniedPage("/access-denied")
         .and().csrf();*/
        http
                .cors()
                .and()
                .csrf()
                .disable()
                .exceptionHandling()
                .authenticationEntryPoint(unauthorizedHandler)
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/api/auth/**")
                .permitAll()
//                .antMatchers("/api/users/**")
//                .permitAll()
                .anyRequest()
                .authenticated();

        http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);

        //HTTPS configuration. It needs to add a few extra settings. Please refer http://www.baeldung.com/spring-channel-security-https for details.
        //http.requiresChannel().anyRequest().requiresSecure();

    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
