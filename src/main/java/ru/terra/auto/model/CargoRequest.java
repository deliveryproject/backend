package ru.terra.auto.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import ru.terra.auto.model.dict.LoadType;
import ru.terra.auto.model.dict.VehicleDict;
import ru.terra.auto.model.embedded.CargoInfo;
import ru.terra.auto.model.embedded.RouteInfo;

/**
 * @author Evgeniy Kobtsev
 */
@Entity
@Table(name = "CARGO_REQUEST")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class CargoRequest extends AbstractEntity {

	@Embedded
	private CargoInfo cargoInfo;

	@Embedded
	private RouteInfo routeInfo;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "USER_ID", nullable = false)
	private User user;

    @ManyToMany(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinTable(name = "CARGO_REQ_LOAD_TYPE_DICT", joinColumns = {
            @JoinColumn(name = "CARGO_REQ_ID", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "LOAD_TYPE_ID",
                    nullable = false, updatable = false) })
	private List<LoadType> supportedLoadTypes;

    @ManyToMany(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinTable(name = "CARGO_REQ_VEHICLE_DICT", joinColumns = {
            @JoinColumn(name = "CARGO_REQ_ID", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "VEHICLE_DICT_ID",
                    nullable = false, updatable = false) })
    private List<VehicleDict> supportedVehicles;

	@Column(name = "PAYMENT_INFO")
	private String paymentInfo;

	@Column(name = "COMMENT")
	private String comment;

}
