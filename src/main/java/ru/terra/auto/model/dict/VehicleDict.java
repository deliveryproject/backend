package ru.terra.auto.model.dict;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import ru.terra.auto.model.embedded.Dimensions;
import ru.terra.auto.model.enums.VehicleBodyType;

/**
 * Created by zhogin on 12/7/2016.
 */
@Entity
@Table(name = "VEHICLE_DICT")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class VehicleDict extends AbstractDict {

    @Column(name = "MANUFACTURER")
    private String manufacturer;

    @Column(name = "MODEL")
    private String model;

    @Column(name = "MODEL_YEAR")
    private Integer modelYear;

    @Column(name = "C_CAPACITY")
    private Double standardCarryingCapacity;

    @Column(name = "BODY_TYPE")
    @Enumerated
    private VehicleBodyType standardBodyType;

    @Embedded
    private Dimensions standardDimensions;

}
