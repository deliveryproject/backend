package ru.terra.auto.model.dict;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Evgeniy Kobtsev
 */
@Entity
@Table(name = "POINT_DICT")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class Point extends AbstractDict {

    @Column(name = "AREA_NAME")
    private String areaName;

    @Column(name = "AREA_CODE")
    private Integer areaCode;

}
