package ru.terra.auto.model.dict;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Evgeniy Kobtsev
 */
@MappedSuperclass
@Data
@NoArgsConstructor
public abstract class AbstractDict {

	@Id
	@Column(name = "ID")
	@GeneratedValue
	private Long id;

	/**
	 * String value of dictionary.
	 */
	@Column(name = "TITLE")
	private String title;


}
