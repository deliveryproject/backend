package ru.terra.auto.model.enums;

import lombok.Getter;

/**
 * Created by zhogin on 12/13/2016.
 */
public enum UserRoleType {

    USER("ROLE_USER"),
    ADMIN("ROLE_ADMIN");

    @Getter
    String roleName;

    UserRoleType(String roleName){
        this.roleName = roleName;
    }

}
