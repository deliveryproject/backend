package ru.terra.auto.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by zhogin on 12/13/2016.
 */
@Entity
@Table(name = "USER_ROLES")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@ToString(exclude = "users")
public class UserRole extends AbstractEntity {

    @Column(name = "USER_ROLE")
    private String roleName;

    @ManyToMany
    @JoinTable(name = "APP_USERS_ROLES", joinColumns = {
            @JoinColumn(name = "ROLE_ID")},
            inverseJoinColumns = {@JoinColumn(name = "USER_ID")})
    private List<User> users;

}
