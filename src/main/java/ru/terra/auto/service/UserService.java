package ru.terra.auto.service;

import java.util.List;

import ru.terra.auto.dto.RegistrationUserDto;
import ru.terra.auto.dto.UserDto;
import ru.terra.auto.model.UserRole;

/**
 * Created by zhogin on 11/23/2016.
 */
public interface UserService extends GenericService<UserDto> {
    UserDto getByUsername(String username);

    RegistrationUserDto getByUsernameWithPassword(String username);

    List<UserRole> getUserRolesById(Long id);

    void saveNewUser(RegistrationUserDto userDTO);
}
