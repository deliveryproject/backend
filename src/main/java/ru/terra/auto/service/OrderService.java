package ru.terra.auto.service;

import ru.terra.auto.dto.OrderDto;
import ru.terra.auto.dto.RegistrationUserDto;
import ru.terra.auto.dto.UserDto;
import ru.terra.auto.model.UserRole;

import java.util.List;

/**
 * @author Denis Zhogin
 */
public interface OrderService extends GenericService<OrderDto> {

}
