package ru.terra.auto.repository;

import org.springframework.stereotype.Repository;

import ru.terra.auto.model.User;

/**
 * @author Evgeniy Kobtsev
 */
@Repository("userRepository")
public interface UserRepository extends GenericRepository<User> {
    User findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}
