package ru.terra.auto.repository.dict;

import org.springframework.stereotype.Repository;

import ru.terra.auto.model.dict.LoadType;

/**
 * @author Evgeniy Kobtsev
 */
@Repository
public interface LoadTypeRepository extends DictGenericRepository<LoadType> {
}
