package ru.terra.auto.repository.dict;

import org.springframework.stereotype.Repository;

import ru.terra.auto.model.dict.Point;

/**
 * @author Evgeniy Kobtsev
 */
@Repository
public interface PointRepository extends DictGenericRepository<Point> {
}
