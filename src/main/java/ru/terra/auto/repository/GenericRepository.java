package ru.terra.auto.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import ru.terra.auto.model.AbstractEntity;

/**
 * Created by zhogin on 11/23/2016.
 */
@NoRepositoryBean
public interface GenericRepository<T extends AbstractEntity> extends CrudRepository<T, Long> {
}
